<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\EnterForm;
use achertovsky\math\models\Math;

class GameController extends Controller
{
    /**
     * Main action of our game
     * @return string
     */
    public function actionIndex()
    {
        $math_source = new Math();

        $request = Yii::$app->request;
        $session = Yii::$app->session;

        //Quantity of rounds and operation which we will use
        $rounds_quantity = 3;
        $operation_list =  ['add' => '+', 'sub' => '-'];

        $session->open();

        //We will randomize new values only when user don't submit his answer
        if(empty($request->post())) {
            $a = rand(1,10);
            $b = rand(1,10);

            //Get random operation from our operation list
            $operation_key = array_rand($operation_list, 1);
            $operation = $operation_list[$operation_key];
        }
        else {
            //Get saved randomize values and operation from user session
            $a = $session->get("a");
            $b = $session->get("b");
            $operation_key = $session->get("key");
            $operation = $operation_list[$operation_key];
        }

        $task = $a . $operation . $b;

        //Add params for view
        $params = ['model' => new EnterForm(), 'task' => $task, 'total_rounds' => $rounds_quantity];

        if(!empty($request->post())) {
            //We consider quantity of rounds and add to user session
            $round = $session->get("round");
            !is_null($round) ? $round = $round + 1 : $round = 1;
            $session->set("round", $round);

            //Get result of operation from Math module
            $result = $math_source->$operation_key($a, $b);
            $user_result = $request->post()['EnterForm']['result'];

            //Get user score from session
            $score = $session->get("score");

            //Check, is user's answer correct
            if($result != $user_result) {
                $answer = 'incorrect';
            }
            else {
                $answer = 'correct';
                $score = $score + 1;
                $session->set("score", $score);
            }

            //Check, is this the end of game
            if($round == $rounds_quantity) {
                $params['total_score'] = $score;
                $session->destroy();
            }
            $params['answer'] = ['round' => $round, 'result' => $result, 'answer' => $answer];
            return $this->render('index', $params);
        }
        //Add randomize values and operation to user session
        $session->set("a", $a);
        $session->set("b", $b);
        $session->set("key", $operation_key);

        return $this->render('index', $params);
    }
}