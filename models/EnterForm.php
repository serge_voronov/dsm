<?php

namespace app\models;

use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class EnterForm extends Model
{
    public $result;
    public $mode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['result','required'],
            ['result','number']
        ];
    }
}