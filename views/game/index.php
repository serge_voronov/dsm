<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
//Little bit JS in view file. It's only for demo
$script = <<< JS
    $(document).ready(function(){
        $('#enterform-mode').on('click', function(){
            if(this.checked) {
                $('form').attr("data-pjax", 1)
            }
            else {
                $('form').removeAttr("data-pjax");
            }
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

Pjax::begin(); ?>
<div class="game-index">
    <div id="task">
        <?= Html::encode($task) . " = ?" ?>
    </div>

    <?php $form = ActiveForm::begin(); ?>
    <div class="mode">
        <?= $form->field($model, 'mode')->checkbox(['label' => 'Async mode', 'labelOptions' => ['style' => 'padding-left:20px;']]); ?>
    </div>

    <?php if(isset($answer)) {
        echo $form->field($model, 'result')->textInput(['disabled' => true]);
    }
    else {
        echo $form->field($model, 'result') ?>
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
         </div>
     <?php }
     ActiveForm::end(); ?>
</div>
<?php if(isset($answer)) { ?>
    <div class="answer">
        <?= Html::encode($task) . " = " . $answer['result'] . " <span class='answer-icon'><img src='picture/" . $answer['answer'] . ".png'></span>"; ?>
    </div>
    <div class="rounds">
        <?= "Round " . $answer['round'] . " / " . $total_rounds ?>
    </div>
    <?php if($answer['round'] == $total_rounds) { ?>
        <div class="total-score">
            <?php if(is_null($total_score)) $total_score = 0 ?>
            <?= "You have " . $total_score . " right answers. <a href='?r=game/index'>Try yet!</a>" ?>
        </div>
    <?php }
        else { ?>
            <div class="next-round">
                <?= Html::a('Next round', ['/game/index'], ['class'=>'btn btn-primary']) ?>
            </div>
        <?php }
    ?>
<?php } ?>
<?php Pjax::end(); ?>
